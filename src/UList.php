<?php /** @noinspection PhpIllegalPsrClassPathInspection */
/** @noinspection PhpUnused */

namespace uhi67\ulist;

use Exception;
use ReflectionException;
use uhi67\menu\Menu;
use uhi67\uxapp\ArrayUtils;
use uhi67\uxapp\Assertions;
use uhi67\uxapp\BaseModel;
use uhi67\uxapp\DataSourceInterface;
use uhi67\uxapp\DBX;
use uhi67\uxapp\Model;
use uhi67\uxapp\QueryDataSource;
use uhi67\uxapp\UXApp;
use uhi67\uxapp\Util;
use uhi67\uxapp\Module;
use uhi67\uxapp\UXAppException;
use uhi67\uxml\UXMLElement;

/**
 * UList module
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright 2014-2020
 * @license MIT
 */

/**
 * # UList - the pageable, filterable, orderable list widget
 *
 * ## Simple example:
 * ### In Controller
 *
 * ```php
 *	 $sql1 = $sql1 ? $sql1 : "select count(*) from ($sql) x";
 *	 $count = $db->selectvalue($sql1, $params);
 *	 $list = new UList($options);
 *	 if($withSearch) $list->setSearch($search);
 *	 $node_list = $list->createNode($node_parent);
 *	 $sql = $list->sqlTail($sql, 0, $exp);
 *	 $node_parent->query($db, $itemname, $sql, $params);
 * ```
 *
 * #### Options:
 * - UXAppPage **page** -- the controller - mandatory
 * - string **id** -- unique identifier for cache (default is 'list1')
 * - string **name** -- common name for list type (e.g. use paging) (default is 'list')
 * - string **itemName** -- tagname of items (default is 'item')
 * - int **count** -- number of elements - must be calculated before using page data (default is 0)
 * - array **orders** -- numeric indexed array of orders for sql order (default is empty)
 * - int **order** -- default index for order array (default is 0)
 * - array **columns** -- array of _{name,title,ord1,ord2,width1,width2,limit}_. **Shortcuts:**
 *      - _field_ = array(modelclass, fieldname, [max-label-length]) or 'Model:fieldname' generates name (label) and title.
 *      - _ord_ = true generates ord1, ord2 from name; ord=array generates ord1, ord2 and corresponding orders elements
 *      - _width_ = single value for both width1 and width2, or a two-element array for them
 *      > null elements will be ignored.
 * - array **attributes** -- associative array of other attributes to add to list node:
 *      - url = action of list form (default is page url)
 *      - popup -- 1 = called in ajax window: pagination will call sf_select(this, params) instead of post
 *      - width -- width of list table in % or in pixel (simple number)
 *      - divclass -- custom class for containing div
 *      - formclass -- custom class for the form element
 *      - class -- custom class for list table. "client" means client-side ordering -- TODO: nem működik
 *      - columns -- `name,title,ord1,ord2,width1,width2,limit|...|` (deprecated, use columns option)
 *      - selectall -- name of column where 'select all' checkbox appears in the header
 *      - with-navigator -- show navigator bar: 0=no (default), 1=above, 2=above and below
 *      - empty -- text displayed when list is empty.
 *      > (null elements will be ignored.)
 * - array **headerFields** -- fieldname-indexed array of input field definitions displayed above the list header {caption,type,option,value}
 * - array **variables** -- name=>value pairs to generate hidden inputs into list form
 * - {@see DatasourceInterface} **dataSource** -- pageable data source for auto generating list data
 * - bool **withSearch** -- use search with dataSource (see {@see DatasourceInterface::setPattern})
 * - array **modelOptions** -- item node creation options if data source is given (see {@see BaseModel::createNode()})
 * - bool **paging** -- paging is enabled (default is true)
 * - Menu **menu** -- context menu attached to the list, may contain items active on selections
 *
 * ### In XSL view
 *
 * ```XML
 * <xsl:apply-templates select="list[@name='listanév']">       <!-- (névvel, ha több lista is van) -->
 *   <xsl:with-param name="spacing" select="2" />
 * </xsl:apply-templates>
 *
 * <xsl:template match="elemnév" mode="list-item">
 *   <tr>
 *     <td>cella-1</td>...
 *   </tr>
 * </xsl:template>
git c * ```
 *
 * ## Example with multiple selection and context menu:
 *
 *
 *
 * #### XSL callbacks
 * - <template match="@item-name" mode="list-item">
 * - <xsl:template match="list" mode="in-form">			- user tartalom (pl. input hidden) - a form-ban, de listablokk előtt
 * - <xsl:template match="list" mode="views">			- user vezérlők listablokkba, navigátor elé
 * - <xsl:template match="list" mode="filter">			- user vezérlők listablokkba, navigátor elé
 * - <xsl:template match="list" mode="options">			- navigátorba user vezérlők
 * - <xsl:template match="list" mode="before-table">	- table előtt, de navigátor után
 * - <xsl:template match="list" mode="list-start">		- table-ban a tbody előtt
 * - <xsl:template match="list" mode="list-first">		- table-ban az első sor előtt
 * - <xsl:template match="list" mode="list-last">		- table-ban az utolsó sor után
 * - <xsl:template match="list" mode="list-end">		- table-ban a tbody után
 * - <xsl:template match="list" mode="list-empty"/>		- table után, ha nincs eleme, a kiírt szöveg
 * - <xsl:template match="list" mode="end-list">		- table után, alsó navigátor előtt
 * - <xsl:template match="list" mode="end-form">		- alsó navigátor után, listablokk után, form vége előtt
 * - <xsl:template match="list" mode="after-form">		- form után
 *
 * ## Stretch rules
 *
 * 1. Add ˙'class' => 'stretch'˙ to the `'attributes' => ` of the UList object. This will activate the auto-resize script.
 * 2. Add `'width' => [120,400]` to the variable width column definitions with min and max width. Use single value for fix-width columns.
 * 3. Add `<div class="fix stretch" style="width:114px">` to the `<td>` cells of the variable width columns using 6px less with than the minimum.
 * Note: The div receives the actual width from the stretch script runtime.
 *
 * {@see __construct() }
 *
 * @uses Menu
 * @property mixed orderName
 */
class UList extends Module {
    // Configuration options
    /** @var int $defaultPageLength -- configured default */
    public $defaultPageLength;
    /** @var string $id -- Unique identifier of this list instance including all parameters (for caching, etc. Include username if matters) */
    public $id = 'list1';
    /** @var string $name -- Name of this kind of list in the application */
    public $name = 'list';
    /** @var string $itemName -- Name of item tags to display, default is data source model name or 'item' */
    public $itemName = null;
    /** @var int $count -- Number of all items in the data source */
    public $count = 0;
    /** @var string[] $orders -- Numeric indexed array of all available orders for column-dependent ordering (usually alternating asc and desc variants)*/
    public $orders = array();
    /** @var int $order -- index of current order (set the default, but may change) */
    public $order = 0;
    /** @var array[] $attributes -- Other attributes to add to node (UList does not use them, except columns) */
    public $attributes;
    /** @var array[] $columns -- array of column definitions: {field/name,title,ord1,ord2,width1,width2,limit}  */
    public $columns;
    /** @var array[] $headerFields -- fieldname-indexed array of field definitions displayed in the list header {caption,type,value} */
    public $headerFields;
    /** @var mixed[] $variables -- name=>value pairs to create hidden inputs in the list form */
    public $variables = array();
    /** @var DataSourceInterface $dataSource -- data source if provided */
    public $dataSource;
    /** @var string $withSearch -- use search with dataSource */
    public $withSearch;
    /** @var array $modelOptions -- item node creation options if data source is given */
    public $modelOptions;
    /** @var bool $paging -- set to false to switch off paging (default is on) */
    public $paging = true;
    /** @var Menu $menu -- menu connected to the list */
    public $menu;
    /** @var array $scriptParams -- Lapozáskor az url-hez fűzendő paraméterek. {@see setScriptParam()} állítja be */
    public $scriptParams = array();

    /** @var bool $req_list -- True, if this list has been submitted */
    private $req_list = false;
    /** @var int $pageShow -- Number of current page */
    private $pageShow;
    /** @var int $pageLength -- Number of rows on a page */
    private $pageLength;
    /** @var int $pageCount -- Number of pages */
    private $pageCount;
    /** @var bool $showSearch -- Display search input (default is false) */
    private $showSearch = false;
    /** @var string $search -- Search pattern */
    private $search;
    /** @var string $script -- Az oldalhoz rendelt script, ha ismer. A célja csak annyi, hogy az XML-be beleteszi. */
    private $script;
    /** @var int $_userPageLength -- user page length preference -- @see $userPageLength property */
    private $_userPageLength;

    /**
     * Komponens attributumok beállítása utáni előkészítés
     *
     * @throws Exception
     */
    public function prepare() {
        parent::prepare();

        $this->script = $this->page->getPath(0);
        $this->req_list = $this->page->app->request->req('list_name') == $this->name;
        if($this->req_list) {
            $_SESSION['list_length_' . $this->name] = $this->pageLength;
            $_SESSION['page_length'] = $this->pageLength; // Erre a sessionre mindig az utolsó lapbeállítás a default
            $this->search = $this->page->app->request->req('list_search');
            $_SESSION['list_search_' . $this->name] = $this->search;
        }

        if(is_array($this->columns)) $this->columns = array_filter($this->columns);
        if(is_array($this->attributes)) $this->attributes = array_filter($this->attributes);

        if($this->dataSource) {
            if($this->withSearch) {
                $this->dataSource->setPattern($this->setSearch(), array('fieldName' => $this->fieldName()));
            }
            $this->count = $this->dataSource->count;

            if($this->modelOptions === null) $this->modelOptions = array();

            // ### Default értékek ###
            // nodeName --> item_name
            if(isset($this->modelOptions['nodeName']) && !$this->itemName)
                $this->itemName = $this->modelOptions['nodeName'];
            // modelClass --> item_name
            if(!$this->itemName && $this->dataSource instanceof QueryDataSource && $this->dataSource->modelClass) {
                $this->itemName = Util::uncamelize(Util::shortName($this->dataSource->modelClass), '-');
            }
            // item_name --> nodeName
            if(!isset($this->modelOptions['nodeName'])) {
                if($this->itemName) $this->modelOptions['nodeName'] = $this->itemName;
            }
        }

        // Computes width1, width2 from width; ord1, ord2 from ord; name, title from field
        $db = ($this->dataSource instanceof QueryDataSource) ? $this->dataSource->query->connection : null;
        UXApp::trace(['db' => $db]);
        if($this->columns) {
            foreach ($this->columns as &$column) {
                $fieldName = '';
                if(isset($column['field'])) {
                    $field = $column['field'];
                    // Breaking up 'class:field' notation
                    if(is_string($field) && strpos($field, ':')) $field = explode(':', $field);
                    // Using datasource model for single fieldname
                    if(is_string($field) && $this->dataSource && $this->dataSource->modelClass) $field = array($this->dataSource->modelClass, $field);
                    if(is_array($field) && is_a($field[0], BaseModel::class, true)) {
                        $className = $field[0];
                        $fieldName = $field[1];
                        $label = call_user_func(array($className, 'attributeLabel'), $fieldName);
                        if(isset($field[2])) {
                            $labelLength = (int)$field[2];
                            if(mb_strlen($label) > $labelLength) $label = mb_substr($label,0, $labelLength-1).'.';
                        }
                        $column['name'] = $label;
                        $column['title'] = call_user_func(array($className, 'attributeHint'), $fieldName, 'hint');
                    }
                    else throw new Exception('Invalid field definition in the columns of the List: '.Util::objtostr($field));
                }
                if(isset($column['width'])) {
                    $width = $column['width'];
                    if(is_numeric($width)) {
                        $column['width1'] = (int)$width;
                        $column['width2'] = (int)$width;
                    }
                    else if(is_array($width)) {
                        $column['width1'] = $width[0];
                        $column['width2'] = $width[1];
                    }
                }

                if(isset($column['ord'])) {
                    $ord = $column['ord'];
                    if ($ord === true && $fieldName) {
                        $column['ord1'] = array($fieldName);
                        $column['ord2'] = array(array($fieldName, DBX::ORDER_DESC));
                    } else if (is_array($ord) && count($ord)==2 && is_scalar($ord[0]) && isset($this->orders[$ord[0]])) {
                        $column['ord1'] = $this->orders[$ord[0]];
                        $column['ord2'] = $this->orders[$ord[1]] ?? DBX::desc([$column['ord1']]);
                    } else if (is_array($ord)) {
                        $column['ord1'] = $ord;
                        $column['ord2'] = DBX::desc($ord);
                    } else if(!is_bool($ord)) {
                        $column['ord1'] = array($ord);
                        $column['ord2'] = DBX::desc(array($ord));
                    }
                }
                if(isset($column['ord1']) && is_array($ord1 = $column['ord1'])) {
                    $this->orders[] = $db ? $db->buildOrderList($ord1) : $ord1;
                    if(!array_key_exists('ord2', $column)) $column['ord2'] = DBX::desc($ord1);
                    $column['ord1'] = count($this->orders)-1;
                }
                if(isset($column['ord2']) && is_array($ord2 = $column['ord2'])) {
                    $this->orders[] = $db ? $db->buildOrderList($ord2) : $ord2;
                    $column['ord2'] = count($this->orders)-1;
                }
            }
        }

        if(is_array($this->orders)) {
            $this->order = $this->page->app->session->get('list_order_'.$this->name, $this->order);
            if($this->req_list && $this->page->app->request->req('list_order')!='') $this->order = $this->page->app->request->getInt('list_order');
            if($this->req_list) $_SESSION['list_order_'.$this->name] = $this->order;
        }
        $this->createPageLength();
        $this->impose();
        $this->page->view->addXslCall('list-init', 1); // it must be called once at body start
    }

    /**
     * Processes runtime actions when request contains act=module
     *
     * @return void
     */
    function action() {}

    /**
     * Sets the number of elements of the list.
     *
     * @param int $count
     *
     * @return void
     * @throws UXAppException
     */
    public function setCount($count) {
        $this->count = $count;
        $this->createPageLength();
        $this->impose();
    }

    /**
     * @param int $n
     *
     * @throws UXAppException
     */
    public function setPageLength($n) {
        $this->pageLength = $n;
        $this->createPageLength();
        $this->impose();
    }

    function setScript($script) {
        $this->script = $script;
    }

    /**
     * Returns page_length preference of current user
     *
     * @return integer
     */
    public function getUserPageLength() {
        if($this->_userPageLength) return $this->_userPageLength;
        $configlength = class_exists('App') ? $this->defaultPageLength : 50;
        $user_page_length = $this->page->app->session->get('page_length', $this->page->app->session->get('user_page_length', $configlength));
        if($user_page_length<1) $user_page_length = $configlength;
        return $this->_userPageLength = $user_page_length;
    }

    /**
     * Recalculates page_length, page_show;
     * Stores paging parameters into the session
     *
     * @return void
     * @throws UXAppException
     */
    private function createPageLength() { // setCount után ezt is újra kell számolni, ugyanis impose elrontja a page_show értékét.
        if($this->paging) {
            Assertions::assertString($this->name);
            $this->pageShow = (int)($this->req_list ? $this->page->app->request->get('list_page') : $this->page->app->session->get('list_page_' . $this->name, 1));
            $this->pageLength = $this->pageLength ? $this->pageLength : (int)($this->req_list ? $this->page->app->request->get('list_length') : $this->page->app->session->get('list_length_' . $this->name, static::getUserPageLength()));
        } else {
            $this->pageShow = 1;
            $this->pageLength = $this->count;

        }
        if($this->req_list) {
            $_SESSION['list_length_' . $this->name] = $this->pageLength;
            $_SESSION['page_length'] = $this->pageLength; // Erre a sessionre mindig az utolsó lapbeállítás a default
//            $this->search = $this->page->app->request->req('list_search');
            $_SESSION['list_search_' . $this->name] = $this->search;
        }
    }

    /**
     * Recalculates page_count, page_show
     *
     * Modifies page_length if is out of range.
     *
     * @return void
     */
    private function impose() {
        if($this->paging) {
            if($this->pageLength < 1) $this->pageLength = static::getUserPageLength(); else if($this->pageLength > 500) $this->pageLength = static::getUserPageLength();
            #Debug::tracex('count', "$this->count, $this->page_count");
            if($this->count % $this->pageLength > 0) $this->pageCount = (int)($this->count / $this->pageLength) + 1;
            else $this->pageCount = (int)($this->count / $this->pageLength);
            if($this->pageShow > $this->pageCount) $this->pageShow = $this->pageCount;
            if($this->pageShow < 1) $this->pageShow = 1;
        } else {
            $this->pageLength = $this->count;
            $this->pageCount = 1;
            $this->pageShow = 1;
        }
        if($this->pageShow != 1)
            $_SESSION['list_page_' . $this->name] = $this->pageShow;
        else
            unset($_SESSION['list_page_' . $this->name]);
    }

    /**
     * Sets the page number to show
     *
     * If $n is out of the range of the list, the first or last will be set.
     *
     * @param int $n -- value in 1...page_count range
     *
     * @return void
     */
    function setPage($n) {
        $this->pageShow = $n;
        $this->impose();
    }

    /**
     * A listát odalapozza a megadott sorszámú elemhez
     *
     * @param mixed $pos - hányadik elemhez
     *
     * @return void
     */
    function jump($pos) {
        $this->setPage((int)(($pos-1) / $this->pageLength)+1);
    }

    function setScriptParam($name, $value) {
        $this->scriptParams[$name]=$value;
    }

    function getScript() {
        $q = http_build_query($this->scriptParams);
        return $this->script . ($q ? '?'.$q : '');
    }

    function getFirst() {
        return ($this->pageShow-1) * $this->pageLength + 1;
    }

    function getLast() {
        return $this->pageShow * $this->pageLength;
    }

    /**
     * Creates <list> node
     *
     * @param UXMLElement $node_parent - Node to create list node under. Deafault is page/content
     *
     * @return UXMLElement the list node
     * creates <list list-name item-name count order page-count page page-length item-first item-last search columns>
     * @throws ReflectionException
     * @throws Exception
     */
    public function createNode($node_parent=null) {
        $node_parent = $this->parentNode($node_parent);
        if(!$node_parent) throw new Exception('Invalid parent node');
        $node = $node_parent->addNode('list', array(
            'script' => $this->getScript(),
            'name' => $this->name,
            'id' => $this->id,
            'item-name' => $this->itemName,
            'count' => $this->count,
            'order' => $this->order,
            'page-count' => $this->pageCount,
            'page' => $this->pageShow,
            'page-length' => $this->pageLength,
            'item-first' => $this->getFirst(),
            'item-last' => $this->getLast(),
        ));
        if($this->showSearch) {
            $node->setAttribute("with-search", 'Y');
            $node->setAttribute("search", $this->search);
        }
        if($this->columns) {
            $columns = ''; // name,title,ord1,ord2,width1,width2,limit|
            foreach($this->columns as $column) {
                /** @var array $field -- field definition to retrieve name and title from model */
                $field = ArrayUtils::getValue($column, 'field');
                $name = '';
                $title = '';
                if(is_array($field) && is_a($field[0], BaseModel::class, true)) {
                    $name = call_user_func(array($field[0], 'attributeLabel'), $field[1]);
                    $title = call_user_func(array($field[0], 'attributeHint'), $field[1], 'hint');
                }
                $columns .=
                    ArrayUtils::getValue($column, 'name', $name) . ',' .
                    ArrayUtils::getValue($column, 'title', $title) . ',' .
                    ArrayUtils::getValue($column, 'ord1', '') . ',' .
                    ArrayUtils::getValue($column, 'ord2', '') . ',' .
                    ArrayUtils::getValue($column, 'width1', '') . ',' .
                    ArrayUtils::getValue($column, 'width2', '') . ',' .
                    ArrayUtils::getValue($column, 'limit', '') . '|';
            }
            $node->setAttribute('columns', $columns);
            unset($this->attributes['columns']);
        }
        if(is_array($this->attributes)) foreach($this->attributes as $name=>$value) $node->setAttribute($name, $value);
        if($this->headerFields) {
            foreach($this->headerFields as $name=> $value) {
                $node->addNode( 'headerfield', array_merge(array('name'=>$name), $value));
            }
        }
        if(is_array($this->variables)) foreach($this->variables as $name=>$value) $node->addNode( 'variable', array('name'=>$name, 'value'=>$value));

        if($this->dataSource) {
            UXApp::trace(['count' => $this->dataSource->count]);
            UXApp::trace(['list' => sprintf('%d, %d, %s', $this->offset(), $this->limit(), Util::objtostr($this->order()))]);
            // Creates data elements
            $items = $this->dataSource->fetch($this->offset(), $this->limit(), $this->order());
            if($this->itemName && !isset($this->modelOptions['nodeName'])) {
                $this->modelOptions['nodeName'] = $this->itemName;
            }
            if($this->dataSource->modelClass) {
                try {
                    Model::createNodes($node, $items, $this->modelOptions);
                }
                catch(\Exception $e) {
                    throw new UXAppException('Unable to create model nodes ', $this->modelOptions, $e);
                }
            }
            else {
                $node->createNodes($items, $this->modelOptions);
            }
        }

        if($this->menu) $this->menu->createNode($node);

        return $this->node = $node;
    }

    /**
     * Set list to searchable, and sets/returns current search pattern
     * @param string p -- sets current pattern if not null
     * @return string
     */
    function setSearch($p=null) {
        if($p!==null) $this->search = $p;
        $this->showSearch = true;
        return $this->search;
    }

    /**
     * Megadja, hogy a kurrens oldal hányadik elemmel kezdődik. 0-based
     *
     * @return integer
     */
    function offset() {
        if(!$this->paging) return null;
        $o = ($this->pageShow-1) * $this->pageLength;
        if($o < 0) $o=0;
        return $o;
    }

    /**
     * Returns current page number. 1-based.
     * @return integer
     */
    function getPage() {
        return $this->pageShow;
    }

    /**
     * Returns current maximum number of items per page
     * @return integer
     */
    function limit() {
        if(!$this->paging) return null;
        return $this->pageLength;
    }

    /**
     * Returns current order including order direction
     * @return string
     */
    function getOrderName() {
        if(!$this->orders) return null;
        return isset($this->orders[$this->order]) ? $this->orders[$this->order] : $this->orders[$this->order&1];
    }

    /**
     * Returns index of current order. Odd numbers are the opposite (descending) orders
     * @return int
     */
    function getOrder() {
        return $this->order;
    }

    /**
     * Returns current order as order array for Query
     * Example: converts 'domain, name desc, variant nulls first' to array('domain', array('name', DBX::ORDER_DESC), array('variant', DBX::NULLS_FIRST))
     * @see Query::buildOrders()
     * @return array|null
     */
    function order() {
        if(!$this->orderName) return null;
        if(is_array($this->orderName)) return $this->orderName;
        $order1 = explode(',', $this->orderName);
        foreach($order1 as &$oe) {
            $oe = trim($oe);
            $fieldname = Util::substring_before($oe, ' ', true);
            $qualifiers = strtolower(trim(Util::substring_after($oe, ' ')));
            if($qualifiers) {
                $dir = null;
                $nls = null;
                if(preg_match('/\bdesc\b/', $qualifiers)) $dir = DBX::ORDER_DESC;
                if(preg_match('/\basc\b/', $qualifiers)) $dir = DBX::ORDER_ASC;
                if(preg_match('/\bnulls first\b/', $qualifiers)) $nls = DBX::NULLS_FIRST;
                if(preg_match('/\bnulls last\b/', $qualifiers)) $nls = DBX::NULLS_LAST;
                $oe = array($fieldname, $dir, $nls);
            }
        }
        return $order1;
    }

    /**
     * Returns search field name based on order
     * @return string
     */
    function fieldName() {
        $order = $this->getOrderName();
        if(!$order) return null;
        $oa = explode(',', $this->getOrderName());
        return str_replace(' desc', '', trim($oa[0]));
    }

    /**
     * adds order, limit, offset to SQL string according to list parameters
     *
     * @param string $sql
     * @param int $start -- if true, first order will be "start desc"
     * @param int $all -- if true, all records (skips offset and limit)
     * @return string
     */
    function sqlTail($sql='', $start=0, $all=0) {
        $startx = $start ? 'start desc,' : '';
        $sql .= " order by $startx ".$this->getOrderName();
        if(!$all) $sql .= " limit ".$this->limit()." offset ".$this->offset();
        return $sql;
    }

    /**
    * Adds desc elements to order names array
    *
    * @param array $orders
    * @return array
    */
    static function descOrders($orders) {
        $r = array();
        for($i=0;$i<count($orders);$i++) {
            $r[] = $orders[$i];
            #$r[] = $orders[$i].' desc';
            $r[] = self::descField(trim($orders[$i]));
        }
        return $r;
    }

    /**
     * Reverses one or more order clauses
     *
     * Multiple orders are separated by `,`
     *
     * @param string $order
     * @return string
     */
    static function descField($order) {
        $f2 = array();
        $fields = explode(',', $order);
        for($i=0;$i<count($fields);$i++) {
            $field = trim($fields[$i]);
            if($field=='') return '';
            $words = explode(' ', $field);
            $wn = count($words);
            // Először a nullkezelés
            if($wn>2 && strtolower($words[$wn-2])=='nulls') {
                if(strtolower($words[$wn-1])=='first')$words[$wn-1]='last';
                else if(strtolower($words[$wn-1])=='last')$words[$wn-1]='first';
            }
            if($wn<2) $words[]='desc';
            else if(strtolower($words[1])=='desc') array_splice($words, 1, 1);
            else array_splice($words, 1, 0, 'desc');
            $f2[$i] = implode(' ', $words);
        }
        return implode(', ', $f2);
    }

    /**
     * Megállapítja, hogy a keresett értéket tartalmazó sor hányadik az sql-ben lekérdezett készletben, és oda lapoz.
     * A keresett értéket a készlet első oszlopában keresi. Az első előfordulást adja.
     * Az SQL-hez az aktuális ORDER-t hozzáteszi a lista beállításai alapján.
     *
     * @param string $item -- a keresett érték
     * @param DBX $db
     * @param string $sql -- az SQL, mely a teljes készlet keresett oszlopát adja vissza, ORDER nélkül
     * @param array $params -- SQL paraméterei, ha paraméteres az SQL
     *
     * @return bool -- true, ha megtaláltuk, false, ha nem.
     */
    function jumpitem($item, $db, $sql, $params) {
        $sql = $sql." order by ".$this->orderName;
        $aa = $db->select_column($sql, $params);
        $i = array_search($item, $aa);
        if($i===false) return false;
        $this->jump($i);
        return true;
    }
}
