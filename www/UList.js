/**
    UList module for UXApp
    UList.js
*/

/**
 * @return {void|boolean}
 */
$.fn.UList = function(options) {
    if(options==='allSelected') {
        var allsel = true;
        $('input.selectrow', this).each(function(){if(!$(this).prop('checked')) allsel=false;});
        return allsel;
    }
    if(options==='refreshOperations') {
        var list = $(this);
        var scope = list.parents('form.ulist');
        var selected = list.data('selected'); 	// Kijelölt szamok készlete
        if(selected.length>0) {
            $('.block_operation', scope).show().trigger('popup-show');
            $('.nonblock_operation', scope).hide();
        }
        else {
            $('.block_operation', scope).hide().trigger('popup-hide');
            $('.nonblock_operation', scope).show();
        }

    }

};
// noinspection JSUnusedLocalSymbols
/**
 * @type {{running: boolean, stretchTable: UList.stretchTable, rowClick: UList.rowClick, allClick: UList.allClick, runagain: boolean}}
 */
var UList = {
    running: false,
    runagain: false,
    /**
     *
     * @param event
     * @return {boolean|*}
     */
    rowClick: function(event) {
        var cb = $(this);
        if(this.nodeName.toLowerCase() !== 'input') {
            $cb = $(this).parent().find('input.selectrow');
            $cb.data('shiftKey', event.shiftKey);
            $cb.data('ctrlKey', event.ctrlKey);
            console.log(this.nodeName, $cb);
            return $cb.trigger('click'); //{shiftKey: event.shiftKey, ctrlKey: event.ctrlKey}
        }
        console.log(cb, event);
        var szam = parseInt(cb.data('num'));
        var list = cb.closest('table.list'); // Konkrét lista
        var max = parseInt($('input.selectrow', list).last().data('num')); // Legnagyobb kijelölhető szám
        var lastsel = parseInt(list.data('lastsel')); 	// Utoljára kijelölt szam
        var selected = list.data('selected'); 	// Kijelölt szamok készlete
        var i,j;

        if(cb.prop('checked')) {
            cb.parents('tr').addClass('selected');
            if(event.shiftKey || cb.data('shiftKey')) {
                if(szam !== lastsel) {
                    var d = szam < lastsel ? -1 : 1;
                    for(i=lastsel+d; i !== szam && i>=0 && i<=max;i=i+d) {
                        $('input.selectrow', list).filter(function(){return $(this).data('num')===i;}).prop('checked', true).parents('tr').addClass('selected');
                        j = selected.indexOf(i); if(j===-1) selected.push(i);
                    }
                }
                selected.push(szam);
            }
            else if(event.ctrlKey || cb.data('ctrlKey')) {
                selected.push(szam);
            }
            else {
                $('input.selectrow', list).filter(function() {
                    return parseInt($(this).data('num')) !== szam;
                }).prop('checked', false).parents('tr').removeClass('selected');
                selected = [szam];
            }
            if(list.UList('allSelected')) $('input.selectall', list).prop('checked', true);
        }
        else {
            cb.parents('tr').removeClass('selected');
            if(event.shiftKey) {
                if(szam !== lastsel) {
                    var d1 = szam < lastsel ? -1 : 1;
                    for(i=lastsel;i !== szam && i>=0 && i<=max;i=i+d1) {
                        $('input.selectrow', list).filter(function() {
                            return parseInt($(this).data('num')) === i;
                        }).prop('checked', false).parents('tr').removeClass('selected');
                        j = selected.indexOf(i); if(j !== -1) selected.splice(j,1);
                    }
                }
                j = selected.indexOf(szam); if(j !== -1) selected.splice(j,1);
            }
            else if(event.ctrlKey) {
                var i1 = selected.indexOf(szam); if(i1 !== -1) selected.splice(i1,1);
            }
            else {
                $('input.selectrow', list).filter(function() {
                    return parseInt($(this).data('num')) !== szam;
                }).prop('checked', false).parents('tr').removeClass('selected');
                selected = [];
            }
            $('input.selectall', list).prop('checked', false);
        }
        lastsel = szam;
        //console.log('['+selected.join(',')+']');
        list.data('lastsel', lastsel);
        list.data('selected', selected);
        list.UList('refreshOperations');
        return true;
    },

    allClick: function(event) {
        var cb = $(this);
        var list = cb.parents('table.list'); // Konkrét lista
        var max = parseInt($('input.selectrow', list).last().data('num')); // Legnagyobb kijelölhető szám
        var selected = list.data('selected'); 	// Kijelölt szamok készlete
        var i,j;
        //console.log('selectall');
        // Mindent kijelöl (vagy töröl)
        if(list.UList('allSelected')) {
            // Mindent töröl
            for(i=0;i<=max;i++) {
                $('input.selectrow', list).filter(function() {
                    return parseInt($(this).data('num')) === i;
                }).prop('checked', false).parents('tr').removeClass('selected');
                j = selected.indexOf(i); if(j !== -1) selected.splice(j,1);
            }
        }
        else {
            // Mindent kijelöl
            for(i=0;i<=max;i++) {
                $('input.selectrow', list).filter(function() {
                    return parseInt($(this).data('num')) === i;
                }).prop('checked', true).parents('tr').addClass('selected');
                j = selected.indexOf(i); if(j === -1) selected.push(i);
            }
        }
        list.data('selected', selected);
        list.UList('refreshOperations');
    },

    /**
     * Adjusts table's column widths to max width using <col> limits
     *
     * @param index integer -- not used (for foreach use only)
     * @param table DOMElement -- table to adjust
     */
    stretchTable: function(index, table) {
        //console.log('try', table);
        if(UList.running) return true; // TODO: lockolás táblánként
        UList.running = true;
        //console.log('stretch', $(table).closest('form').attr('id'));
        var width = $(table).css('width'); // tényleges szélesség // TODO: nem látható tab füleken nem működik!
        if(width === '100%') return true;
        width = parseInt(width.replace(/px/,''));
        var parentwidth = parseInt($(table).parent().css('width').replace(/px/,''));

        if(parentwidth>=240) width = Math.min(width, parentwidth);
        if(width===0) width = parentwidth;

        //console.log('stretch to', width, parentwidth, $(table).closest('form').attr('id'));

        var cols = $(table).find('col');
        var wa = [];
        var m0, m1;
        var min = 0;
        var max = 0;
        var lim = 0; // minimum table width to display column. Under this, column will be hidden.
        for(var i=0; i<cols.length; i++) {
            //var minmax = cols.eq(i).attr('width').split(/,/);
            var widths = cols.eq(i).data('width');
            /** @var {array} minmax */
            var minmax = (typeof widths === 'string' && widths>'') ? widths.split(/,/) : [0,0];
            wa[i] = {
                'min': m0 = minmax[0] ? parseInt(minmax[0]) : 0,
                'max': m1 = minmax[1] ? parseInt(minmax[1]) : m0,
                'lim': lim = minmax[2] ? parseInt(minmax[2]) : 0
            };
            if(lim===0 || width>=lim) {
                min += m0;
                max += m1;
            }
            else {
                // Hide columns under limit
                $(table).find('tr').each(function(){
                    $(this).find('td,th').eq(i).hide();
                    wa[i].hidden = true;
                    //console.log('Col#'+i+' is hidden');
                });
            }
        }

        var refreshColumns = function(){
            width = $(table).css('width');
            width = parseInt(width.replace(/px/,'')) - cols.length * 5;
            width = Math.max(width, parentwidth); // tényleges szélesség újramérve rejtés után

            var d = (max - min); // Teljes táblázat tágulási határai (az adott oszlopkonfigurációban, rejtéseket már figyelembe véve)
            if(d === 0) {
                console.log('StrecthTable: No enough space for minimum column widths.');
                return false;}
            var r = (width - min) / d; // A szükséges tágulási együttható: 0=min, 1=max
            for(var i=0; i<cols.length; i++) {
                //var lim = wa[i].lim;
                // Ignore hidden columns
                if(wa[i].hidden) continue;
                var s = wa[i].max - wa[i].min; // mozgástér a mezőben
                var w = wa[i].min + Math.floor(s*r); // A mező szükséges mérete a minimumhoz képest
                var rr = s * r / wa[i].min + 1; // mező szélességi szorzója
                cols.eq(i).attr('width', w-2);
                // Cellákon belüli elemek átméretezése
                $(table).find('tr').each(function(){
                    $(this).find('td,th').eq(i).find('.stretch').each(function(){
                        var style = $(this).attr('style');
                        var matches = style ? style.match(/width:\s*(\d+)px/) : null;
                        var aw = matches ? parseInt(matches[1]) : 100;
                        var nw = Math.floor((aw+7) * rr)-7;
                        $(this).css('width', ''+nw+'px');
                    });
                });
            }
            if(UList.runagain) {
                UList.runagain = false;
                $('table.stretch').each(UList.stretchTable);
            }
            UList.running = false;
        };
        setTimeout(refreshColumns(), 100);
        return true;
    }
};

/* onload */
$(function() {
    $('div.dialog').dialog({
        autoOpen:false,
        modal: true,
        buttons: {Ok: function() {$(this).dialog('close')}}
    });

    /* Többszörös kijelölések */
    var lists = $('table.list'); // Listák készlete, amelyeken a funkció függetlenül működik
    lists.each(function(){
        $(this).data('selected', [])	// Kiválasztott id-k tömbje
            .data('lastsel', null);			// Utolsó kiválasztott id
        // Minden sort ellátunk egyesével növekvő sorszámmal
        var rows = $('input.selectrow', this);
        for(var i=0;i<rows.length;i++) {
            rows.eq(i).data('num', i);
        }
    });
    $('.selectrow', lists).on('click', {boudData: 'test'}, UList.rowClick);
    $('input.selectall', lists).on('click', UList.allClick);

    // Ha az url-ben # van, akkor jump oda
    var rowpos = $('form#form_'+document.location.hash.substr(1)).position();
    if(rowpos) $('html, body').scrollTop(rowpos.top);

    $('.list_search').keypress(function(evt) {
        const $form = $(this).closest('form');
        evt = evt || window.event;
        const code = evt.keyCode || evt.which;
        if(code===13) return list_search_start($form.get(0));
    });
    // noinspection JSUnusedLocalSymbols
    $('.list_length').keypress(function(evt) {
        var name = this.id.split('_')[1];

        evt = evt || window.event;
        var code = evt.keyCode || evt.which;
        if(code===13) return list_submit(name);
    });

    /* Automatikus oszlop-méretezés */
    setTimeout(function() {
        //console.log($('table.stretch'));
        $('table.stretch').each(UList.stretchTable);
    }, 200);

    /* Selectrow magyarázat */
    var title = 'Kattintással a sor kijelölhető vagy a kijelölés visszavonható. Többszörös kijelölés Ctrl gomb nyomása mellett, tartomány kijelölése Shift gombbal.';
    $('input.selectrow', lists).first().parent().data('title', title).attr('id', 'ulist-selectrow').addClass('uxapp-tooltip');

    /* headerben lévő checkbboxok hidden párjának frissítése (hogy kikapcsolt checkbox esetén is küldjön) */
    $('input.ulist-header-flag').change(function() {
        $('input#_'+this.id).val(this.checked ? this.value : 0);
    });

});


// noinspection JSUnusedGlobalSymbols
function list_setpage(formname, oldal) {
    var form = jQuery('#form_list_'+formname);
    //console.log(formname);
    form.find('#list_page').val(oldal);
    form.submit();
    return false;
}

/* oszlop rendező nyílra kattintottak */
// noinspection JSUnusedLocalSymbols, JSUnusedGlobalSymbols
function list_setorder(formname, ord, button) {
    var form = $('#form_list_'+formname);
    var $table = form.find('table').first();
    if($table.hasClass('client')) {
        // Kliens oldali
        var $th = $(button).closest('th');
        var colnum = $th.prevAll('th').length+1;
        console.log('client order ', colnum);
        // Rendezés iránya
        var dir = 'down';
        var $sp = $(button).find('span.btn');
        if($sp.hasClass('r_down')) dir = 'up';
        // Előző rendező nyíl eltüntetése
        $(button).closest('tr').find('th span.btn').removeClass('r_down r_up').addClass('r_updown');
        // lokális rendezés az oszlop szerint tr.data sorokat
        $first = $('tr.data', $table).first();
        if($first) {
            var coldata = $('tr.data td:nth-of-type(' + colnum + ')', $table).map(function (i, e) {
                return [[$(e).text(), e.parentNode]];
            }).get();
            coldata.sort(function (a, b) {
                if(dir==='down') {
                    if (!parseInt(a[0]) || !parseInt(a[0])) return a[0].toLowerCase() < b[0].toLowerCase();
                    return a[0] < b[0];
                } else {
                    if (!parseInt(a[0]) || !parseInt(a[0])) return a[0].toLowerCase() > b[0].toLowerCase();
                    return a[0] > b[0];
                }
            }); // Növekvő
            //console.log(coldata);
            var $placeholder = $('<tr>').insertBefore($first);
            for(var i = 0; i < coldata.length; i++) {
                $placeholder.after(coldata[i][1]);
            }
            $placeholder.remove();
            // Rendező nyíl kirajzolása
            $sp.removeClass('r_updown').addClass('r_'+dir);
        }
    }
    else {
        // Szerver oldali
        form.find('#list_order').val(ord);
        form.submit();
    }
    return false;
}

// noinspection JSUnusedGlobalSymbols
function list_setlength(formname, pagelength) {
    var $form = $('#form_list_'+formname);
    $form.find('length').val(pagelength);
    $form.submit();
    return false;
}

function list_submit(formname) {
    $('#form_list_'+formname).submit();
    return false;
}

// noinspection JSUnusedGlobalSymbols
function list_search_start(form) {
    var $form = $(form);
    if($form.find('#list_search').val() > ' '
        || !($form.find('#_list_checkpattern'))
        || $form.find('#user-flags input').length > 0) {
        $form.find('#list_go').val(1);
        $form.find('#list_page').val(1);
        $form.submit();
    }
    else {
        console.log(form.find('#search_hint'));
        $('#search_hint').dialog('open');
    }
    return false;
}

// noinspection JSUnusedGlobalSymbols
function list_search_cancel(formname) {
    var form = $('#form_list_'+formname);
    form.find('#list_search').val('');
    form.find('#list_go').val(0);
    form.submit();
    return false;
}

// noinspection JSUnusedGlobalSymbols
function list_export(formname) {
    var $form = $('#form_list_'+formname);
    $form.attr('target', '_blank');
    $form.find('#list_exp').val(1);
    $form.submit();
    $form.find('#list_exp').val('');
    $form.removeAttr('target');
}
