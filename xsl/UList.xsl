<?xml version="1.0" encoding="UTF-8"?>
<!-- UList.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/1999/xhtml">
<xsl:output method="xml" version="1.0" indent="yes"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
 />
<!--
    Requirements:
        Uses Menu module for contex-menus (or define template match="menu" mode="context-menu")
 -->

    <!--
        Callbacks:
        - <template match="@item-name" mode="list-item">
        - <xsl:template match="list" mode="in-form">		- user tartalom (pl. input hidden) - a form-ban, de listablokk előtt
        - <xsl:template match="list" mode="views">			- user vezérlők listablokkba, navigátor elé
        - <xsl:template match="list" mode="filter">			- user vezérlők listablokkba, navigátor elé
        - <xsl:template match="list" mode="options">		- navigátorba user vezérlők
        - <xsl:template match="list" mode="before-table">	- table előtt, de navigátor után
        - <xsl:template match="list" mode="list-start">		- table-ban a tbody előtt
        - <xsl:template match="list" mode="list-first">		- table-ban az első sor előtt
        - <xsl:template match="list" mode="list-last">		- table-ban az utolsó sor után
        - <xsl:template match="list" mode="list-end">		- table-ban a tbody után
        - <xsl:template match="list" mode="list-empty"/>	- table után, ha nincs eleme, a kiírt szöveg
        - <xsl:template match="list" mode="end-list">		- table után, alsó navigátor előtt
        - <xsl:template match="list" mode="end-form">		- alsó navigátor után, listablokk után, form vége előtt
        - <xsl:template match="list" mode="after-form">		- form után

        - <xsl:template match="menu" mode="context-menu">	- in Menu module or define elsewhere
     -->

    <xsl:template match="list">
        <xsl:param name="with-navigator" select="@with-navigator" />		<!-- 0,1,2 -->
        <xsl:param name="action"/>							<!-- form action (default a hívó oldal) -->
        <xsl:param name="popup" select="@popup" /> 			<!-- ha 1, akkor lapozáskor post helyett sf_select(this, params) -->
        <xsl:param name="width" select="@width" />			<!-- listaszélesség % vagy anélkül pixelben értelmezve -->
        <xsl:param name="divclass" select="@divclass" />	<!-- custom class a befoglaló divnek, ha kell -->
        <xsl:param name="class" select="@class" />			<!-- custom class a lista táblázatnak, ha kell. "client"=kliens oldali rendezés. -->
        <xsl:param name="columns" select="@columns" />		<!-- name,title,ord1,ord2,width1,width2,limit|...| -->
        <xsl:param name="selectall" select="@selectall" />	<!-- oszlop neve, amelyben a minden sor kijelölése megjelenik -->
        <a name="a_{@name}" />
        <xsl:variable name="url">
            <xsl:choose>
                <xsl:when test="@url"><xsl:value-of select="@url" /></xsl:when>
                <xsl:when test="/data/@url"><xsl:value-of select="/data/@url" /></xsl:when>
                <xsl:otherwise><xsl:value-of select="$script" /></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:comment>UList: list</xsl:comment>
        <form action="{$url}#a_{@name}" name="list_{@name}" id="form_list_{@name}" method="post" accept-charset="UTF-8" class="ulist {@formclass}">
            <xsl:if test="$popup=1"><xsl:attribute name="class">popup</xsl:attribute></xsl:if>
            <xsl:if test="$action"><xsl:attribute name="action"><xsl:value-of select="$action"/>#a_{@name}</xsl:attribute></xsl:if>
            <xsl:apply-templates select="variable" />
            <xsl:apply-templates select="." mode="in-form"/>
            <input type="hidden" id="list_v" name="list_v" value="{@view}" />
            <input type="hidden" id="list_name" name="list_name" value="{@name}" />
            <input type="hidden" id="list_page" name="list_page" value="{@page}" />

            <!-- top menu -->
            <xsl:apply-templates select="menu[@position='top' or not(@position)]" mode="context">
                <xsl:with-param name="divid">context-menu-list</xsl:with-param>
            </xsl:apply-templates>
            <xsl:if test="not(menu)">
                <xsl:apply-templates select="../menu[@position='top' or not(@position)]" mode="context">
                    <xsl:with-param name="divid">context-menu-list</xsl:with-param>
                </xsl:apply-templates>
            </xsl:if>

            <div class="list-external uxapp-tooltip {$divclass}" id="ulist-list" data-title="Általános lista, lehet lapozható, rendezhető vagy kereshető is.">
                <xsl:choose>
                    <xsl:when test="contains($width,'%') or contains($width,'px')">
                        <xsl:attribute name="style">width:<xsl:value-of select="$width" /></xsl:attribute>
                    </xsl:when>
                    <xsl:when test="$width">
                        <xsl:attribute name="style">width:<xsl:value-of select="$width" />px;</xsl:attribute>
                    </xsl:when>
                </xsl:choose>
                <xsl:apply-templates select="." mode="views" />
                <xsl:apply-templates select="." mode="filters" />
                <xsl:if test="$with-navigator &gt; 0">
                    <xsl:apply-templates select="." mode="navigator" />
                </xsl:if>
                <xsl:if test="$columns != ''">
                    <input type="hidden" id="list_order" name="list_order" value="{@order}" />
                </xsl:if>
                <xsl:apply-templates select="." mode="before-table"/>
                <table class="list {$class}">
                    <xsl:choose>
                        <xsl:when test="contains($width,'%') or contains($width,'px')">
                            <xsl:attribute name="style">width:<xsl:value-of select="$width" /></xsl:attribute>
                        </xsl:when>
                        <xsl:when test="$width">
                            <xsl:attribute name="style">width:<xsl:value-of select="$width" />px;</xsl:attribute>
                        </xsl:when>
                    </xsl:choose>

                    <!-- header row -->
                    <xsl:if test="$columns != ''">
                        <xsl:call-template name="columns">
                            <xsl:with-param name="columns" select="$columns" />
                            <xsl:with-param name="selectall" select="$selectall" />
                        </xsl:call-template>
                    </xsl:if>

                    <xsl:apply-templates select="." mode="list-start"/>
                    <tbody>
                        <xsl:apply-templates select="." mode="list-first"/>
                        <xsl:apply-templates select="*[name()=current()/@item-name]" mode="list-item" />
                        <xsl:if test="not(*[name()=current()/@item-name])">
                            <xsl:apply-templates select="../*[name()=current()/@item-name]" mode="list-item" />
                        </xsl:if>
                        <xsl:apply-templates select="." mode="list-last"/>
                    </tbody>
                    <xsl:apply-templates select="." mode="list-end"/>
                </table>
                <xsl:comment><xsl:value-of select="current()/@item-name" />=<xsl:value-of select="count(../*[name()=current()/@item-name])" /></xsl:comment>
                <xsl:if test="not(@count &gt; 0)">
                    <div class="list-empty">
                        <xsl:apply-templates select="." mode="list-empty"/>
                    </div>
                </xsl:if>
                <xsl:apply-templates select="." mode="end-list"/>
                <xsl:if test="$with-navigator &gt; 1">
                    <xsl:apply-templates select="." mode="navigator2" />
                </xsl:if>
            </div>

            <xsl:apply-templates select="menu[@position='bottom']" mode="context">
                <xsl:with-param name="divid">context-menu-list</xsl:with-param>
            </xsl:apply-templates>
            <xsl:if test="not(menu)">
                <xsl:apply-templates select="../menu[@position='bottom']" mode="context">
                    <xsl:with-param name="divid">context-menu-list</xsl:with-param>
                </xsl:apply-templates>
            </xsl:if>

            <xsl:apply-templates select="." mode="end-form"/>
        </form>
        <xsl:apply-templates select="." mode="after-form"/>
    </xsl:template>

    <xsl:template match="list" mode="navigator">
        <div id="ulist-navigator" class="navigator uxapp-tooltip" data-title="Lapozható lista navigátor sávja.">
            <span style="float:left" class="nav1">
                <span id="ulist-count" class="label uxapp-tooltip" title="Összes rekord száma"><xsl:value-of select="@count"/>&#160;sor</span>
                <input id="list_length" class="list_length" name="list_length" size="1" maxlength="3" value="{@page-length}" />
                <a href="#" onclick="return list_submit('{@name}');">
                    <span class="label">&#160;sor/oldal.</span>
                </a>
                <xsl:if test="@exp=1">
                    <a class="navi" title="Excel export" href="#" onclick="list_export('{@name}'); return false;" style="margin-right: 4px;">
                        <input id="list_exp" type="hidden" name="list_exp" value="" />
                        <i class="fa fa-file-excel-o export"/><!-- <span style="color:#1A4;font-weight:bold;border:1px solid #666;padding:1px 2px;background:#FFF;">X</span> -->
                    </a>
                </xsl:if>
                <span id="user-flags">
                    <xsl:if test="headerfield"><xsl:attribute name="class">rightborder</xsl:attribute></xsl:if>
                    <xsl:apply-templates select="headerfield" />
                    <xsl:apply-templates select="." mode="options" /> <!-- callback -->
                </span>
            </span>
            <xsl:if test="@with-search">
                <div style="float:left" id="ulist-search" class="uxapp-tooltip" title="A listában keres. A megadott keresési mintára illeszkedő rekordokat fogja megmutatni. Az OK gombot meg kell nyomni a kereséshez. A minta reguláris kifejezés is lehet." >
                    <span class="label">&#160;Szűkít:</span>
                    <input id="list_search" class="list_search" name="list_search" maxlength="25" style="width:100px" title="Re"
                           value="{@search}" onfocus="this.select();" />
                    <input type="hidden"  id="list_go" name="list_go" />
                    <a class="navi" title="A rendezett oszlopban keres" href="#" onclick="list_search_start('{@name}'); return false;">
                        <xsl:text>OK</xsl:text>
                    </a>
                    <xsl:if test="@search!=''">
                        <a id="ulist-searchcancel" class="navi uxapp-tooltip" title="Keresés befejezése, visszatérés a teljes listához." href="#" onclick="list_search_cancel('{@name}'); return false;">
                            <xsl:text>| vissza </xsl:text>
                        </a>
                    </xsl:if>
                </div>
            </xsl:if>
            <xsl:call-template name="page-buttons" />
            <span>&#160;</span>
        </div>
    </xsl:template>

    <xsl:template match="headerfield">
        <span class="field">
            <xsl:choose>
                <xsl:when test="@type='hidden'">
                    <input id="{generate-id()}" type="{@type}" name="{@name}" value="1" />
                </xsl:when>
                <xsl:when test="@type='checkbox'">
                    <xsl:variable name="id"><xsl:value-of select="generate-id()" /></xsl:variable>
                    <input id="{$id}" type="{@type}" name="_{@name}" value="1" class="ulist-header-flag">
                        <xsl:if test="@value=1"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                    </input>
                    <input id="_{$id}" type="hidden" name="{@name}" value="{@value}" />
                    <label for="{$id}"><xsl:value-of select="@caption" /></label>
                </xsl:when>
                <xsl:when test="@type='text'">
                    <label for="{generate-id()}"><xsl:value-of select="@caption" /></label>
                    <input id="{generate-id()}" type="{@type}" name="{@name}" value="{@value}" />
                </xsl:when>
                <xsl:when test="@type='select'">
                    <label for="{generate-id()}"><xsl:value-of select="@caption" /></label>
                    <select id="{generate-id()}" name="{@name}">
                        <xsl:for-each select="option">
                            <option value="{@value}"><xsl:value-of select="." /></option>
                        </xsl:for-each>
                    </select>
                </xsl:when>
                <xsl:when test="@type='radio'">
                    <label for="{generate-id()}"><xsl:value-of select="@caption" />:</label>
                    <xsl:for-each select="option">
                        <input name="{../@name}" type="radio" value="{@value}"/><xsl:value-of select="." />&#160;
                    </xsl:for-each>
                </xsl:when>
            </xsl:choose>
        </span>
    </xsl:template>

    <xsl:template match="list" mode="navigator2">
        <div class="navigator uxapp-tooltip" id="ulist-nav2" data-title="Alsó navigátor sáv. Hosszú listáknál alkalmazzuk. Itt csak lapozó gombok vannak.">
            <span style="float:left" class="nav1">
            </span>
            <xsl:call-template name="page-buttons" />
            <span>&#160;</span>
        </div>
    </xsl:template>

    <xsl:template name="page-buttons">
        <span id="ulist-paging" class="navigator uxapp-tooltip" style="float:right" data-title="Lapozó gombok. A pöttyökkel közbülső lapokra is lehet ugrani.">
            <!--
                    <xsl:if test="@page &gt; 1">
                        <a onclick="return list_setpage('{@name}', {@page - 1})" href="#">prev|</a>
                    </xsl:if>
                    <xsl:if test="@page &lt; 2">
                        <span class="dis">prev |</span>
                    </xsl:if>
             -->
            <!-- first -->
            <xsl:if test="@page &lt; 2">
                <i class="btn fa fa-fast-backward disabled"/><!-- <span class="btn first_i" /> -->
            </xsl:if>
            <xsl:if test="@page &gt; 1">
                <a onclick="return list_setpage('{@name}', 1)" href="#" title="első oldalra lapoz">
                    <i class="btn fa fa-fast-backward"/><!-- <span class="btn first" /> -->
                    <span class="nav">1</span>
                </a>
            </xsl:if>

            <xsl:if test="@page &gt; 6">
                <!-- ... -->
                <xsl:variable name="m2" select="round((@page - 1) div 2)" />
                <xsl:variable name="m1" select="round(($m2 + 1) div 2)" />
                <xsl:variable name="m3" select="round(($m2 + @page - 2) div 2)" />
                <a onclick="return list_setpage('{@name}', {$m1})" href="#" title="{$m1}. oldalra lapoz">.</a>
                <a onclick="return list_setpage('{@name}', {$m2})" href="#" title="{$m2}. oldalra lapoz">.</a>
                <a onclick="return list_setpage('{@name}', {$m3})" href="#" title="{$m3}. oldalra lapoz">.</a>
            </xsl:if>
            <xsl:if test="@page &gt; 2">
                <!-- előzők -->
                <xsl:if test="@page = 6">
                    <a onclick="return list_setpage('{@name}', {@page - 4})" href="#"><xsl:value-of select="@page - 4" /></a>
                </xsl:if>
                <xsl:if test="@page = 6 or @page = 5">
                    <a onclick="return list_setpage('{@name}', {@page - 3})" href="#"><xsl:value-of select="@page - 3" /></a>
                </xsl:if>
                <xsl:if test="@page - 2 &gt; 1">
                    <a onclick="return list_setpage('{@name}', {@page - 2})" href="#"><xsl:value-of select="@page - 2" /></a>
                </xsl:if>
                <xsl:if test="@page - 1 &gt; 1">
                    <a onclick="return list_setpage('{@name}', {@page - 1})" href="#"><xsl:value-of select="@page - 1" /></a>
                </xsl:if>
            </xsl:if>
            <!-- current -->
            <span class="nav"><b><xsl:value-of select="@page" /></b></span>
            <xsl:if test="@page &lt; @page-count - 1">
                <!-- következők -->
                <xsl:if test="@page + 1 &lt; @page-count">
                    <a onclick="return list_setpage('{@name}', {@page + 1})" href="#"><xsl:value-of select="@page + 1" /></a>
                </xsl:if>
                <xsl:if test="@page + 2 &lt; @page-count">
                    <a onclick="return list_setpage('{@name}', {@page + 2})" href="#"><xsl:value-of select="@page + 2" /></a>
                </xsl:if>
                <xsl:if test="@page + 5 = @page-count or @page + 4 = @page-count ">
                    <a onclick="return list_setpage('{@name}', {@page + 3})" href="#"><xsl:value-of select="@page + 3" /></a>
                </xsl:if>
                <xsl:if test="@page + 5 = @page-count">
                    <a onclick="return list_setpage('{@name}', {@page + 4})" href="#"><xsl:value-of select="@page + 4" /></a>
                </xsl:if>
            </xsl:if>
            <xsl:if test="@page &lt; @page-count - 5">
                <!-- ... -->
                <xsl:variable name="m2" select="round((@page-count + @page + 2) div 2)" />
                <xsl:variable name="m3" select="round(($m2 + @page-count) div 2)" />
                <xsl:variable name="m1" select="round(($m2 + @page + 2) div 2)" />
                <a onclick="return list_setpage('{@name}', {$m1})" href="#" title="{$m1}. oldalra lapoz">.</a>
                <a onclick="return list_setpage('{@name}', {$m2})" href="#" title="{$m2}. oldalra lapoz">.</a>
                <a onclick="return list_setpage('{@name}', {$m3})" href="#" title="{$m3}. oldalra lapoz">.</a>
            </xsl:if>

            <!-- next -->
            <!--
                    <xsl:if test="@page &lt; @page-count">
                        <a onclick="return list_setpage('{@name}', {@page + 1})" href="#">next</a>
                    </xsl:if>
                    <xsl:if test="@page &gt; @page-count - 1">
                        <span class="dis">next</span>
                    </xsl:if>
             -->

            <!-- last -->
            <xsl:if test="@page &lt; @page-count">
                <a onclick="return list_setpage('{@name}', {@page-count});" href="#" title="Utolsó oldalra lapoz">
                    <span class="nav"><xsl:value-of select="@page-count" /></span>
                    <i class="fa btn fa-fast-forward"/><!-- <span class="btn last" /> -->
                </a>
            </xsl:if>
            <xsl:if test="@page &gt;= @page-count">
                <i class="fa btn fa-fast-forward disabled"/><!-- <span class="btn last_i" /> -->
            </xsl:if>
            &#160;
        </span>
    </xsl:template>

    <!-- title,ord1,ord2,width1,width2,limit|...| -->
    <xsl:template name="columns">
        <xsl:param name="columns" />
        <xsl:param name="selectall" />
        <colgroup>
            <xsl:call-template name="columnwidths"><xsl:with-param name="columns" select="$columns" /></xsl:call-template>
        </colgroup>
        <thead>
            <tr class="list-columns uxapp-tooltip" id="ulist-columns" data-title="Listafejléc">
                <xsl:call-template name="columncells">
                    <xsl:with-param name="columns" select="$columns" />
                    <xsl:with-param name="selectall" select="$selectall" />
                </xsl:call-template>
            </tr>
        </thead>
    </xsl:template>

    <xsl:template name="columnwidths">
        <xsl:param name="columns" />
        <xsl:param name="pos" select="1"/>
        <xsl:if test="contains($columns, '|')">
            <xsl:variable name="col1" select="substring-before($columns, '|')" />
            <xsl:variable name="colx" select="substring-after($columns, '|')" />
            <xsl:variable name="titlex" select="substring-after($col1, ',')" />
            <xsl:variable name="ordx" select="substring-after($titlex, ',')" />
            <xsl:variable name="ord1" select="substring-before($ordx, ',')" />
            <xsl:variable name="ord2w" select="substring-after($ordx, ',')" />
            <xsl:variable name="ord2">
                <xsl:choose>
                    <xsl:when test="contains($ord2w, ',')"><xsl:value-of select="substring-before($ord2w, ',')" /></xsl:when>
                    <xsl:otherwise><xsl:value-of select="$ord2w" /></xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="cw">
                <xsl:choose>
                    <xsl:when test="contains($ord2w, ',')"><xsl:value-of select="substring-after($ord2w, ',')" /></xsl:when>
                    <xsl:otherwise/>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="cw1">
                <xsl:choose>
                    <xsl:when test="contains($cw, ',')"><xsl:value-of select="substring-before($cw, ',')" /></xsl:when>
                    <xsl:otherwise><xsl:value-of select="$cw" /></xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <col width="{$cw1}" id="col_{$pos}" data-width="{$cw}" />
            <xsl:call-template name="columnwidths">
                <xsl:with-param name="columns" select="$colx" />
                <xsl:with-param name="pos" select="$pos+1" />
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <xsl:template name="columncells">
        <xsl:param name="columns" />
        <xsl:param name="selectall" />
        <xsl:if test="contains($columns, '|')">
            <xsl:variable name="col1" select="substring-before($columns, '|')" />
            <xsl:variable name="colx" select="substring-after($columns, '|')" />
            <xsl:variable name="colname">
                <xsl:choose>
                    <xsl:when test="contains($col1,',')"><xsl:value-of select="normalize-space(substring-before($col1, ','))" disable-output-escaping="yes"/></xsl:when>
                    <xsl:otherwise><xsl:value-of select="normalize-space($col1)" /></xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="titlex" select="substring-after($col1, ',')" />
            <xsl:variable name="ordx" select="substring-after($titlex, ',')" />
            <xsl:variable name="ord1" select="substring-before($ordx, ',')" />
            <xsl:variable name="ord2w" select="substring-after($ordx, ',')" />
            <xsl:variable name="ord2">
                <xsl:choose>
                    <xsl:when test="contains($ord2w, ',')"><xsl:value-of select="substring-before($ord2w, ',')" /></xsl:when>
                    <xsl:otherwise><xsl:value-of select="$ord2w" /></xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="cw">
                <xsl:choose>
                    <xsl:when test="contains($ord2w, ',')"><xsl:value-of select="substring-after($ord2w, ',')" /></xsl:when>
                    <xsl:otherwise/>
                </xsl:choose>
            </xsl:variable>
            <xsl:comment><xsl:value-of select="$colname" />;<xsl:value-of select="$selectall" /></xsl:comment>
            <th>
                <xsl:if test="$colname=$selectall and $selectall!='' or $colname='' and $selectall='.'">
                    <div id="ulist-selectall" data-title="Az összes sort kijelöli vagy a kijelölést megszünteti" class="uxapp-tooltip inline">
                        <input class="selectall" type="checkbox" title="mindet kijelöli" />
                    </div>
                </xsl:if>
                <xsl:if test="$ord1!=''">
                    <xsl:variable name="orderx">
                        <xsl:choose>
                            <xsl:when test="@order=$ord1"><xsl:value-of select="$ord2"/></xsl:when>
                            <xsl:otherwise><xsl:value-of select="$ord1"/></xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <a href="#{$orderx}" title="rendezés" onclick="return list_setorder('{@name}', '{$orderx}', this);">
                        <xsl:choose>
                            <xsl:when test="@order=$ord1"><span id="ulist_down" class="btn r_down fa fa-sort-desc uxapp-tooltip"  title="Növekvően rendezett oszlop. Kattintson a nyílra a csökkenő rendezéshez."/></xsl:when>
                            <xsl:when test="@order=$ord2"><span id="ulist_up" class="btn r_up fa fa-sort-asc uxapp-tooltip" title="Csökkenően rendezett oszlop. Kattintson a nyílra a növekvő rendezéshez."/></xsl:when>
                            <xsl:otherwise><span id="ulist_updown" class="btn r_updown fa fa-sort uxapp-tooltip" title="Rendezhető oszlop. Kattintson a nyílra a rendezéshez."/></xsl:otherwise>
                        </xsl:choose>
                    </a>
                </xsl:if>
                <span title="{substring-before($titlex, ',')}">
                    <xsl:value-of select="$colname" disable-output-escaping="yes"/>&#160;
                </span>
            </th>
            <xsl:call-template name="columncells">
                <xsl:with-param name="columns" select="$colx" />
                <xsl:with-param name="selectall" select="$selectall" />
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    <!-- ==================================================================================== -->
    <xsl:template match="column"> <!-- ../rendez rendez rendez-desc  -->
        <th id="ulist_column">
            <xsl:if test="position()=1">
                <xsl:attribute name="class">uxapp-tooltip</xsl:attribute>
                <xsl:attribute name="title">Oszlopcímke</xsl:attribute>
            </xsl:if>
            <xsl:choose>
                <xsl:when test="string-length(@order)>0">
                    <div class="resizable">
                        <xsl:variable name="orderx">
                            <xsl:choose>
                                <xsl:when test="@order=../@order"><xsl:value-of select="@order-desc"/></xsl:when>
                                <xsl:otherwise><xsl:value-of select="@order"/></xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <a href="#" title="rendezés" onclick="document.{../@name}.sort('{$orderx}', '{../navigator/@page}', '{../navigator/@page-length}'); return false;">
                            <xsl:choose>
                                <xsl:when test="../@order=@order"><span id="ulist_down" class="btn r_down uxapp-tooltip" title="növekvően rendezett oszlop. Kattintson a nyílra a csökkenő rendezéshez."/></xsl:when>
                                <xsl:when test="../@order=@order-desc"><span id="ulist_up" class="r_up uxapp-tooltip"  title="csökkenően rendezett oszlop. Kattintson a nyílra a növekvő rendezéshez."/></xsl:when>
                                <xsl:otherwise><span id="ulist_updown" class="r_updown uxapp-tooltip" title="Rendezhető oszlop. Kattintson a nyílra a rendezéshez."/></xsl:otherwise>
                            </xsl:choose>
                        </a>
                        <a class="listcolumn" title="{@hint}">
                            <xsl:value-of select="@caption" disable-output-escaping="yes"/>&#160;
                        </a>
                    </div>
                </xsl:when>
                <xsl:otherwise><div class="listcolumn"><xsl:value-of select="@caption"/></div></xsl:otherwise>
            </xsl:choose>
        </th>
    </xsl:template>

    <xsl:template match="*" mode="after-form">
    </xsl:template>

    <xsl:template match="navigator" mode="after-form">
        <script type="text/javascript" language="JavaScript">
            document.<xsl:value-of select="../@name"/>.submitx =
                new Function("{ if(!this.changed || confirm('Rögzíti az adatokat?')) {if(this.changed) this.action.value='update'; this.submit();}}");
            document.<xsl:value-of select="../@name"/>.sort =
                new Function('rend', "{ this.order_<xsl:value-of select="../@name"/>.value = rend; this.submitx(); }");
            // ez az
        </script>
    </xsl:template>

    <xsl:template match="*" mode="in-form">
    </xsl:template>

    <xsl:template match="*" mode="options">
    </xsl:template>

    <xsl:template match="*" mode="views">
    </xsl:template>

    <xsl:template match="*" mode="filters">
    </xsl:template>

    <xsl:template match="*" mode="before-table">
    </xsl:template>

    <xsl:template match="*" mode="list-first">
    </xsl:template>

    <xsl:template match="*" mode="list-start">
    </xsl:template>

    <xsl:template match="*" mode="list-last">
    </xsl:template>

    <xsl:template match="*" mode="list-end">
    </xsl:template>

    <xsl:template match="*" mode="list-empty">
        <xsl:if test="@empty">
            <xsl:value-of select="@empty"/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="*" mode="end-list">
    </xsl:template>

    <xsl:template match="*" mode="end-form">
    </xsl:template>

    <xsl:template match="list" mode="options">
        <!-- Flag indicating there are no options. On override, it will not present. -->
        <input type="hidden" id="_list_checkpattern" name="_list_checkpattern" value="1" />
    </xsl:template>

    <xsl:template match="xsl-call[@name='list-init']">
        <div id="search_hint" class="dialog hidden">
            Írjon be egy keresendő értéket, és utána nyomja meg a szűkít gombot!
        </div>
    </xsl:template>

    <xsl:template match="variable">
        <input type="hidden" name="{@name}" value="{@value}">
            <xsl:if test="@id"><xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute></xsl:if>
        </input>
    </xsl:template>

</xsl:stylesheet>
