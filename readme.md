UList module for UXApp
======================

### Version 1.0 -- 2021-03-07 

Installation
------------
Add to your composer.json:
```
{
  "require": {
    "uhi67/uxapp-ulist": "*"
  },
  "repositories": [
    {
      "type": "vcs",
      "url": "git@bitbucket.org:uhi67/uxapp-ulist.git"
    }
  ],
}
```

Usage
-----

### In the controller:
```
    $ds = new QueryDataSource(['query' => new Query()]);
	$list = new uhi67\ulist\UList($page, [
        'id' => 'list1_1', 
        'name' => 'item_list', 
        'itemName' => 'item', 
        'dataSource' => $ds]);
	$list->createNode($this->node_content);
```

### In the xml view ###
```xhtml
<div>
    ...
    <!-- The list itself -->
    <xsl:apply-templates select="list" />
    
    <!-- You must specify item views -->
    <xsl:template match="item_name_selector" mode="list-item">
       <tr>
         <td>column_value</td>...
       </tr>
    </xsl:template>
    
    <!-- you may override the notification for the empty list -->
    <xsl:apply-templates select="list" mode="list-empty">
        This list is empty.
    </xsl:apply-templates>
</div>
```

Change log
----------

### 1.0 -- 2021-03-07

- initial release
